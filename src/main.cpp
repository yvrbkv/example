#include <cstdio>
#include <vector>
#include <iostream>

#include "equation.h"
#include "matrix.h"

int main()
{
    Equation *eq;
    int n, reg, ts;
    double dt, dx;

    std::cout << "N = ";
    std::cin >> n;
    std::cout << "Time step = ";
    std::cin >> dt;
    std::cout << "Number of time steps = ";
    std::cin >> ts;
    std::cout << "dx = ";
    std::cin >> dx;
    std::cout << "Regularization: [0] - OFF, [1] - ON (if necessary): ";
    std::cin >> reg;
    //std::cout << "[N] [time step] [number of time steps] [dx] [reg flag]: ";
    //std::cin >> n >> dt >> ts >> dx >> reg;

    n++;
    //std::cout << std::endl;

    eq = new Equation(n, dt, dx, reg); // 100 200 400 800
    eq->Eq_sol(ts);
    eq->Print_info();
    delete eq;

    return 0;
}