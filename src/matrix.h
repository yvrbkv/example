#ifndef SVT_MATRIX_H
#define SVT_MATRIX_H

struct Matrix
{
    Matrix(int size, int bsize);
    Matrix(const Matrix& in);
    ~Matrix();

    int n; // matrix size
    int nb; // block size == number of blocks per axis x or y
    int nz; // number of non-zero elements
    int *Ap;
    int *Ai;
    double *Ax;

    void init(double md, double d1, double du, double dl);

    // |md|d1|..|d2|..|..|..
    // |d1|md|d1|..|d2|..|..
    // |0 |d1|md|d1|..|d2|..
    // ...
    // |d3||..|d1|md|d1|..|d2|..
    // |..
};

#endif //SVT_MATRIX_H