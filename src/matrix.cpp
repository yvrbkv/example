#include "matrix.h"
#include <iostream>

Matrix::Matrix(int size, int bsize) : n(size), nb(bsize)
{
    nz = n + 4 * nb * (nb-1);
    //std::cout << n << ' ' << nb << ' ' << nz << std::endl;
    Ap = new int[n+1];
    Ai = new int[nz];
    Ax = new double[nz];
}

Matrix::Matrix(const Matrix &in)
{
    n = in.n;
    nz = in.nz;
    nb = in.nb;

    Ap = new int[n+1];
    Ai = new int[nz];
    Ax = new double[nz];

    for (int i = 0; i < n+1; i++) {
        Ap[i] = in.Ap[i];
    }
    for (int i = 0; i < nz; i++) {
        Ai[i] = in.Ai[i];
        Ax[i] = in.Ax[i];
    }
}

Matrix::~Matrix()
{
    delete[] Ap;
    delete[] Ai;
    delete[] Ax;
}

void Matrix::init(double md, double d1, double du, double dl)
{
    int k = 0, pk = 0;
    for (int i = 0; i < nb; i++) {
        for (int j = 0; j < nb; j++) {
            Ap[pk++] = k;
            if (i != 0) {
                Ai[k] = i * nb + j - nb;
                Ax[k++] = du;
            }
            if (j != 0) {
                Ai[k] = i * nb + j - 1;
                Ax[k++] = d1;
            }
            Ai[k] = i * nb + j;
            Ax[k++] = md;
            if (j != nb-1) {
                Ai[k] = i * nb + j + 1;
                Ax[k++] = d1;
            }
            if (i != nb-1) {
                Ai[k] = i * nb + j + nb;
                Ax[k++] = dl;
            }
        }
    }
    Ap[pk] = k;
}