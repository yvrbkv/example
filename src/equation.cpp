#include "equation.h"
#include <new>
#include <cmath>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <SuiteSparse/UMFPACK/Include/umfpack.h>

Equation::Equation(int n, double ts, double ddx, int reg_flag) : num_xy(n), dt(ts), dx(ddx), reg(reg_flag)
{
    N = num_xy - 1;
    num_sys = (N-1)*(N-1);
    h = 200. / double(N);
    dy = 1e-3;
    time_steps = 0;
    rhs = new double[num_sys];
    prevsol = new double[num_sys];
    currsol = new double[num_sys];
    x = new double[num_xy];
    y = new double[num_xy];
    sol = new double*[num_xy];
    m = new Matrix(num_sys, N-1);
    for (int i = 0; i < num_xy; i++) {
        sol[i] = new double[num_xy];
    }
    for (int i = 0; i < num_sys; i++) {
        prevsol[i] = 0.;
        currsol[i] = 0.;
    }
    set_coords();
}

Equation::Equation(const Equation &in)
{
    num_xy = in.num_xy;
    num_sys = in.num_sys;
    N = in.N;
    h = in.h;
    reg = in.reg;
    dt = in.dt;
    dx = in.dx;
    dy = in.dy;
    time_steps = in.time_steps;

    rhs = new double[num_sys];
    prevsol = new double[num_sys];
    currsol = new double[num_sys];
    x = new double[num_xy];
    y = new double[num_xy];
    sol = new double*[num_xy];
    m = new Matrix(*(in.m));
    for (int i = 0; i < num_xy; i++) {
        sol[i] = new double[num_xy];
    }
    for (int i = 0; i < num_xy; i++) {
        x[i] = in.x[i];
        y[i] = in.y[i];
    }
    for (int i = 0; i < num_sys; i++) {
        prevsol[i] = in.prevsol[i];
    }
}

Equation::~Equation()
{
    delete[] rhs;
    delete[] prevsol;
    delete[] currsol;
    delete[] x;
    delete[] y;
    delete m;
    for (int i = 0; i < num_xy; i++) {
        delete[] sol[i];
    }
    delete[] sol;
}

void Equation::Eq_sol(int ts)
{
    peh = h / dx;
    cur = dt / h;
    time_steps = ts;

    if (reg) {
        if (peh <= 1./(2*h)) {
            reg = 0;
        }
    }

    sys_init();
    sys_sol();
    get_2D_sol();
}

void Equation::Print_info()
{
    std::cout << "====================================================" << std::endl;
    std::cout << "Number of nodes per axis: " << num_xy << std::endl;
    std::cout << "N: " << N << std::endl;
    std::cout << "dx: " << dx << std::endl;
    std::cout << "dy: " << dy << std::endl;
    std::cout << "Time step: " << dt << std::endl;
    std::cout << "Number of time steps: " << time_steps << std::endl;
    std::cout << "Number of equations in the system: " << num_sys << std::endl;
    std::cout << "Spatial step: " << h << std::endl;
    std::cout << "dt/h: " << dt / h << std::endl;
    std::cout << "Pe_h: " << peh << std::endl;
    std::cout << "Regularization: ";
    if (reg) {
        std::cout << "ON" << std::endl;
    } else {
        std::cout << "OFF" << std::endl;
    }
    std::cout << "====================================================" << std::endl << std::endl;

    std::ofstream fout;

    double max = 0., min = 0.;
    fout.open("sol.dat");
    fout.precision(12);
    for (int i = 0; i < num_xy; i++) {
        for (int j = 0; j < num_xy; j++) {
            fout.width(20);
            fout << std::scientific << x[i] << ' ';
            fout.width(20);
            fout << std::scientific << y[j] << ' ';
            fout.width(20);
            if (sol[i][j] > max) {
                max = sol[i][j];
            }
            if (sol[i][j] < min) {
                min = sol[i][j];
            }
            fout << std::scientific << sol[i][j] << std::endl;
        }
    }
    std::cout << "max = " << max << std::endl;
    std::cout << "min = " << min << std::endl;
    fout.close();
}

//----------------------------------------------------------------------------------------------------------------------

void Equation::set_coords()
{
    for (int i = 0; i < num_xy; i++) {
        x[i] = double(i) * h;
        y[i] = -100. + double(i) * h;
    }
}

void Equation::get_2D_sol()
{
    for (int i = 0; i < num_xy; i++) {
        sol[i][0] = 0.;
        sol[i][N] = 0.;
        if (y[i] > -10. && y[i] < 10.) {
            sol[0][i] = 1.;
            sol[N][i] = 0.;
        } else {
            sol[0][i] = 0.;
            sol[N][i] = 0.;
        }
    }

    for (int i = 0; i < N-1; i++) {
        for (int j = 0; j < N-1; j++) {
            sol[i+1][j+1] = currsol[i * (N-1) + j];
        }
    }
}

void Equation::sys_init()
{
    double A, B, C, D;

    if (reg) {
        A = h * h + h * dt + 2. * dt * (dx + dy);
        B = -dx * dt;
        C = -dt * (h + dx);
        D = -dt * dy;
    } else {
        A = 2. * h * h + 4. * dt * (dx + dy);
        B = dt * (h - 2. * dx);
        C = -dt * (h + 2. * dx);
        D = -2. * dt * dy;
    }

    m->init(A, D, B, C);
}

void Equation::rhs_init()
{
    double C, F;

    if (reg) {
        C = -dt * (h + dx);
        F = h * h;
    } else {
        C = -dt * (h + 2. * dx);
        F = 2. * h * h;
    }

    for (int i = 0; i < N-1; i++) {
        for (int j = 0; j < N-1; j++) {
            rhs[i * (N-1) + j] = F * prevsol[i * (N-1) + j];
            if (i == 0 && y[1+j] > -10. && y[1+j] < 10.) {
                rhs[i * (N-1) + j] -= C;
            }
        }
    }
}

void Equation::sys_sol()
{
    int status;
    double Control[UMFPACK_CONTROL], Info[UMFPACK_INFO];
    void *Symbolic, *Numeric;

    umfpack_di_defaults(Control);

    status = umfpack_di_symbolic(m->n, m->n, m->Ap, m->Ai, m->Ax, &Symbolic, Control, Info);
    if (status != UMFPACK_OK) {
        std::cout << "ERROR: at umfpack_di_symbolic() [error code: " << status << ']' << std::endl;
        exit(1);
    }

    status = umfpack_di_numeric(m->Ap, m->Ai, m->Ax, Symbolic, &Numeric, Control, Info);
    if (status != UMFPACK_OK) {
        std::cout << "ERROR: at umfpack_di_numeric() [error code: " << status << ']' << std::endl;
        exit(1);
    }

    for (int i = 0; i < time_steps; i++) {
        for (int i = 0; i < num_sys; i++) {
            prevsol[i] = currsol[i];
        }
        rhs_init();
        status = umfpack_di_solve(UMFPACK_A, m->Ap, m->Ai, m->Ax, currsol, rhs, Numeric, Control, Info);
        if (status != UMFPACK_OK) {
            std::cout << "ERROR: at umfpack_di_solve() [error code: " << status << ']' << std::endl;
            exit(1);
        }
    }

    umfpack_di_free_symbolic(&Symbolic);
    umfpack_di_free_numeric(&Numeric);

}
