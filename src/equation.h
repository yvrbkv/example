#ifndef SVT_EQUATION_H
#define SVT_EQUATION_H

#include "matrix.h"

class Equation
{
public:
    Equation(int n, double ts, double ddx, int reg_flag);
    Equation(const Equation& in);
    ~Equation();

    void Eq_sol(int ts);
    void Print_info();

private:
    int num_xy; // number of nodes per axis x or y
    int N; // number of last node if their numbers are 0, 1, 2, ... , N
    int num_sys; // number of equations in the system
    int reg; // regularization?
    int time_steps;
    double dt; // time step
    double cur, peh;
    double dx, dy;
    double *x, *y; //coordinates of nodes per axis x or y
    double **sol;
    double *rhs; // right-hand side vector
    double *prevsol, *currsol; // numerical solution
    double h;
    Matrix *m;

    void set_coords();
    void get_2D_sol();
    void sys_init();
    void rhs_init();
    void sys_sol();

};


#endif //SVT_EQUATION_H
